# TensorFlow

TensorFlow 설치

1. CUDA 설치
```bash
    #https://developer.nvidia.com/cuda-release-candidate-download ( gtx1080을 사용하므로 CUDA 8 사용 )
    sudo dpkg --install cuda-repo*
    sudo apt-get update
    sudo apt-get install cuda

    vi ~/.bashrc
        export PATH=/usr/local/cuda/bin:$PATH
        export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH

    source ~/.bashrc
    #해당 터미널 종료 후 반영..
    
    sudo add-apt-repository ppa:graphics-drivers/ppa
    sudo apt-get update
    sudo apt-get install nvidia-367 # gtx1080 기준
```

2. cuDNN 설치
```bash
    #https://developer.nvidia.com/cudnn 에 로그인 후 버전과 OS를 보고 다운로드 한다.
    
    tar xvzf cudnn*
    
    sudo cp cuda/include/cudnn.h /usr/local/cuda/include/
    sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64/
    sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
```

3. NVIDIA CUDA Profile Tools Interface Library
```bash
    sudo apt-get install libcupti-dev
```

4. PYTHON & ANACONDA 설치
```bash
    # python 설치
    sudo add-apt-repository ppa:jonathonf/python-3.6
    sudo apt-get update
    sudo apt-get install python3.6

    # anaconda 설치
    # https://www.continuum.io/downloads 에서 OS에 맞는 installer 다운로드
    bash Anaconda3-4.4.0-Linux-x86_64.sh
```

5. TensorFlow 설치
```bash
    conda create -n tensorflow

    source activate tensorflow

    pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.2.1-cp36-cp36m-linux_x86_64.whl
    # 뒤에 주소는 https://www.tensorflow.org/install/install_linux#the_url_of_the_tensorflow_python_package 에서 python 버전과 GPU 사용유무로 맞는 url로 변경한다.
```

6. Keras 설치
```bash
    sudo pip install keras
```

7. Opencv 설치
```bash
    conda install -c menpo opencv
```

8. Jupyter 실행
```bash
    # 최상의 폴더로 가서 아래의 명령어를 입력하는 것을 권장
    jupyter notebook --generate-config
    
    sudo vi ~/.jupyer/.jupyter_notebook_config.py
        c.NotebookApp.ip = 'IP로'
        c.NotebookApp.open_browser = False
        c.NotebookApp.port = 8888
        c.NotebookApp.password = u'해싱된 패스워드'

    nohup jupyter notebook --allow-root &> ~/jupyter.log
```
```python
    # 해싱된 패스워드 얻는 방법
    from IPython.lib import passwd
    password = passwd("패스워드")
    print password
```
